using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Model animation touch controller
/// </summary>
public class ModelAnimPlayer : MonoBehaviour
{
    /// <summary>
    /// Animator component
    /// </summary>
    [SerializeField]
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            if (touch.fingerId == 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    anim.SetTrigger("PlayAnim");
                }
            }
        }
    }
}
