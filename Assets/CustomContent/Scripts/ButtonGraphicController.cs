using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Changes button icons
/// </summary>
public class ButtonGraphicController : MonoBehaviour
{
    /// <summary>
    /// List of useable sprites
    /// </summary>
    [SerializeField]
    private Sprite[] spriteList;

    //Target image to change icon at
    private Image image;

    //Current state of icon
    private bool state;

    private void Start()
    {
        image = GetComponent<Image>();
    }

    /// <summary>
    /// Switch between first and second image
    /// </summary>
    public void ToggleGraphics()
    {
        state = !state;

        if (state)
        {
            image.sprite = spriteList[0];
        }
        else
        {
            image.sprite = spriteList[1];
        }
    }
}
