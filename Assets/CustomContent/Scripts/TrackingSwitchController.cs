using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Events;

/// <summary>
/// Is responsible for switching between Plane and Image tracking
/// </summary>
public class TrackingSwitchController : MonoBehaviour
{
    [SerializeField]
    private ARPlaneManager PlaneManager;
    [SerializeField]
    private ARTrackedImageManager ImageManager;
    [SerializeField]
    private ARSession Session;

    /// <summary>
    /// Utility pre-switching action event starter
    /// </summary>
    [SerializeField]
    private UnityEvent PreSwitchEvent;
    /// <summary>
    /// Utility post-switching action event starter
    /// </summary>
    [SerializeField]
    private UnityEvent PostSwitchEvent;

    void Awake()
    {
        PlaneManager = GetComponent<ARPlaneManager>();
        ImageManager = GetComponent<ARTrackedImageManager>();
        //Session = GetComponent<ARSession>();
    }

    /// <summary>
    /// Switch between Image and Plane tracking modes
    /// </summary>
    public void SwitchModes()
    {
        PreSwitchEvent.Invoke();

        Session.Reset();

        PlaneManager.enabled = !PlaneManager.enabled;
        ImageManager.enabled = !ImageManager.enabled;

        

        PostSwitchEvent.Invoke();
    }
}
