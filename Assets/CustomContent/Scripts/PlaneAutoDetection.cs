using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

/// <summary>
/// Detects AR plane and places prefab at it
/// </summary>
public class PlaneAutoDetection : MonoBehaviour
{
    /// <summary>
    /// Plane Detection Manager to get update info from
    /// </summary>
    [SerializeField]
    private ARPlaneManager PlaneManager;

    /// <summary>
    /// The prefab to spawn on plane
    /// </summary>
    [SerializeField]
    private GameObject modelPrefab;

    /// <summary>
    /// Instance of spawned prefab
    /// </summary>
    [SerializeField]
    private GameObject prefabInstance;

    void Awake()
    {
        PlaneManager = GetComponent<ARPlaneManager>();
        PlaneManager.planesChanged += PlaneChangedEvent;
    }

    /// <summary>
    /// Event actions to place and update prefab on plane
    /// </summary>
    /// <param name="args"></param>
    private void PlaneChangedEvent(ARPlanesChangedEventArgs args)
    {
        Debug.Log("Change event called");
        if (args.added != null && prefabInstance == null)
        {
            Debug.Log("Placed model on plane");
            ARPlane arPlane = args.added[0];
            prefabInstance = Instantiate(modelPrefab, arPlane.transform.position, Quaternion.identity);
        }
    }

    /// <summary>
    /// Assigned to switch tracking modes button, to be somewhat modular
    /// </summary>
    public void Clear()
    {
        if (prefabInstance != null)
        {
            Destroy(prefabInstance);
        }
    }
}
